package com.reciclapp.DataModel

import com.google.firebase.database.DataSnapshot
import kotlin.jvm.internal.Ref

//Snapshot bjeto específico de Firebase
class CentroReciclaje (snapShot: DataSnapshot){

    lateinit var id: String
    lateinit var latitud: String
    lateinit var longitud: String

    init{
        try{
            val data: HashMap<String, Any> = snapShot.value as HashMap<String, Any>
            id = snapShot.key ?: ""
            if(data["latitud"] !=null) {
                latitud = data["latitud"] as String
            }
            else
            {
                latitud="NA"
            }
            if(data["longitud"]!=null) {
                longitud = data["longitud"] as String
            }
            else
            {
                longitud="NA"
            }
        }
        catch (e: Exception){
            e.printStackTrace()
        }
    }
}
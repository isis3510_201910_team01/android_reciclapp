package com.reciclapp.DataModel

import com.google.firebase.database.DataSnapshot

class Usuario (snapShot: DataSnapshot) {

    lateinit var id: String
    lateinit var puntosActuales: String
    lateinit var puntosAcumulados: String

    init{
        try{
            val data: HashMap<String, Any> = snapShot.value as HashMap<String, Any>
            id = snapShot.key ?: ""
            if(data["puntosActuales"] !=null) {
                puntosActuales = data["puntosActuales"] as String
            }
            else
            {
                puntosActuales="NA"
            }
            if(data["puntosAcumulados"]!=null) {
                puntosAcumulados = data["puntosAcumulados"] as String
            }
            else
            {
                puntosAcumulados="NA"
            }
        }
        catch (e: Exception){
            e.printStackTrace()
        }
    }
}
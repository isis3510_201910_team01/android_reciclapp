package com.reciclapp.DataModel

import com.google.firebase.database.DataSnapshot

class Tip (snapShot: DataSnapshot){

    lateinit var id: String
    lateinit var recomendacion: String

    init{
        try{
            val data: HashMap<String, Any> = snapShot.value as HashMap<String, Any>
            id = snapShot.key ?: ""
            if(data["recomendacion"] !=null) {
                recomendacion = data["recomendacion"] as String
            }
            else
            {
                recomendacion="NA"
            }
        }
        catch (e: Exception){
            e.printStackTrace()
        }
    }
}
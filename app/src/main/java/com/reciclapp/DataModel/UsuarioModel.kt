package com.reciclapp.DataModel

import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import java.util.*

object UsuarioModel: Observable() {

    private var mValueDataListener: ValueEventListener? = null
    private var mUsuario: Usuario? =null

    private fun getDataBaseRef(): DatabaseReference? {
        return FirebaseDatabase.getInstance().reference.child("usuarios").child(FirebaseAuth.getInstance().currentUser!!.uid)
    }

    init {

        //FirebaseDatabase.getInstance().setPersistenceEnabled(true)

        if (mValueDataListener != null)
        {
            getDataBaseRef()?.removeEventListener(mValueDataListener!!)
        }

        mValueDataListener = null
        Log.i("UsuarioModel", "data init linea 27")

        mValueDataListener = object: ValueEventListener{
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                try {
                    Log.i("UsuarioModel", "Actualización data linea 31")
                    if(dataSnapshot != null){
                        val u: Usuario=Usuario(dataSnapshot)
                        mUsuario=u
                    }
                    Log.i("UsuarioModel", "Data actualizada" + mUsuario?.puntosActuales)
                    setChanged()
                    notifyObservers()

                } catch (e: Exception){
                    e.printStackTrace()
                }
            }

            override fun onCancelled(p0: DatabaseError) {
                    Log.i("UsuarioModel", "Linea 56 actualización de data cancelada, err=${p0.message}")
            }
        }

        getDataBaseRef()?.addValueEventListener(mValueDataListener as ValueEventListener)
    }

    fun getData(): String?{
        return mUsuario?.puntosActuales
    }

    fun getPuntosAcumulados(): String?{
        return mUsuario?.puntosAcumulados
    }
}
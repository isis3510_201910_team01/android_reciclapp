package com.reciclapp.DataModel

import android.content.Context
import android.util.Log
import com.google.firebase.database.*
import java.util.*
import kotlin.collections.ArrayList
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences



//Cuando ocurra un cambio en la cache del modelo de centros de reciclaje queremos que la UI lo note.
object CentroReciclajeModel: Observable() {


    private var mValueDataListener: ValueEventListener? = null
    private var mCentrosList: ArrayList<CentroReciclaje>? = ArrayList()

    private fun getDataBaseRef(): DatabaseReference? {
        return FirebaseDatabase.getInstance().reference.child("centros")
    }

    init {

        //FirebaseDatabase.getInstance().setPersistenceEnabled(true)

        if (mValueDataListener != null)
        {
            getDataBaseRef()?.removeEventListener(mValueDataListener!!)
        }

        mValueDataListener = null
        Log.i("CentrosReciclajeModel", "data init linea 27")

        mValueDataListener = object: ValueEventListener{
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                try {
                    Log.i("CentroReciclajeModel", "Actualización data linea 34")
                    val data: ArrayList<CentroReciclaje> = ArrayList()
                    if(dataSnapshot != null){
                        //Se itera sobre la información que se recibió
                        for (snapShot: DataSnapshot in dataSnapshot.children){
                            if(snapShot!=null) {
                                try {
                                    val c: CentroReciclaje=CentroReciclaje(snapShot)
                                    if(c.latitud!=null && c.longitud!=null) {
                                        data.add(CentroReciclaje(snapShot))
                                    }
                                } catch (e: Exception) {
                                    e.printStackTrace()

                                }
                            }
                        }
                    }
                    mCentrosList=data
                    Log.i("CentroReciclajeModel", "Data actualizada, hay " + mCentrosList!!.size + " en cache")
                    setChanged()
                    notifyObservers()
                } catch (e: Exception){
                    e.printStackTrace()
                }
            }

            override fun onCancelled(p0: DatabaseError) {
                if(p0 != null){
                    Log.i("CentroReciclajeModel", "Linea 51 actualización de data cancelada, err=${p0.message}")
                }
            }
        }

        getDataBaseRef()?.addValueEventListener(mValueDataListener as ValueEventListener)
    }

    fun getData(): ArrayList<CentroReciclaje>?{
        return mCentrosList
    }
}
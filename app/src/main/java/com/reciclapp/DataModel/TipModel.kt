package com.reciclapp.DataModel

import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import java.util.*

object TipModel: Observable() {

    private var mValueDataListener: ValueEventListener? = null
    private var mTip: Tip? =null

    private fun getDataBaseRef(): DatabaseReference? {
        val rnds = (1..3).random()
        val r:String = "r"+rnds
        return FirebaseDatabase.getInstance().reference.child("recomendaciones").child(r)
    }

    init {

        //FirebaseDatabase.getInstance().setPersistenceEnabled(true)

        if (mValueDataListener != null)
        {
            getDataBaseRef()?.removeEventListener(mValueDataListener!!)
        }

        mValueDataListener = null
        Log.i("TipModel", "data init linea 27")

        mValueDataListener = object: ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                try {
                    Log.i("TipModel", "Actualización data linea 31")
                    if(dataSnapshot != null){
                        val t: Tip=Tip(dataSnapshot)
                        mTip=t
                    }
                    Log.i("TipModel", "Data actualizada" + mTip?.recomendacion)
                    setChanged()
                    notifyObservers()
                } catch (e: Exception){
                    e.printStackTrace()
                }
            }

            override fun onCancelled(p0: DatabaseError) {
                Log.i("TipModel", "Linea 56 actualización de data cancelada, err=${p0.message}")
            }
        }

        getDataBaseRef()?.addValueEventListener(mValueDataListener as ValueEventListener)
    }

    fun getData(): String?{
        return mTip?.recomendacion
    }

}
package com.reciclapp.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.reciclapp.DataModel.Usuario
import com.reciclapp.R

class UsuarioAdapter(context: Context, resource: Int, list: ArrayList<Usuario>): ArrayAdapter<Usuario>(context, resource, list) {

    private var mResource: Int = 0
    private var mList: ArrayList<Usuario>
    private var mLayoutInflater: LayoutInflater
    private var mContext: Context = context

    init {
        this.mResource = resource
        this.mList = list
        this.mLayoutInflater = mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val returnView: View?
        if(convertView == null) {
            returnView = try {
                mLayoutInflater.inflate(mResource, null)
            } catch (e: Exception) {
                e.printStackTrace()
                View(context)
            }
            setUI(returnView, position)
            return returnView
        }
        setUI(convertView, position)
        return convertView
    }

    private fun setUI(view: View, position: Int) {
        val usuario: Usuario? = if (count > position) getItem(position) as Usuario? else null
        val usuarioPuntos: TextView? = view.findViewById(R.id.textPuntos)
        usuarioPuntos?.text = usuario?.puntosActuales ?: ""
    }
}
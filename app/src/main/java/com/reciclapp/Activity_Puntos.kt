package com.reciclapp

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import com.reciclapp.DataModel.UsuarioModel
import kotlinx.android.synthetic.main.activity__puntos.*
import java.util.*

class Activity_Puntos : AppCompatActivity(), Observer {

    lateinit var puntos: String
    lateinit var puntosAcumulados: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity__puntos)

        UsuarioModel.addObserver(this)

        puntos= UsuarioModel.getData().toString()
        Log.i("hhh", puntos)

        puntosAcumulados= UsuarioModel.getPuntosAcumulados().toString()

        if(puntos!="null")
        {
            val usuarioPuntos: TextView? = this.findViewById(R.id.textPuntos)
            usuarioPuntos?.text = puntos

            val usuarioNivel: TextView? = this.findViewById(R.id.textNivel)
            usuarioNivel?.text = ((Integer.parseInt(puntosAcumulados)/50)+1).toString()
        }

        but_inicio.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            // start your next activity
            startActivity(intent)
        }

        but_centros.setOnClickListener {
            val intent = Intent(this, MapsActivity::class.java)
            // start your next activity
            startActivity(intent)
        }

        but_camara.setOnClickListener {
            val intent = Intent(this, Activity_Camara::class.java)
            // start your next activity
            startActivity(intent)
        }

        imageView6.setOnClickListener {
            val intent = Intent(this, AliadosActivity::class.java)
            // start your next activity
            startActivity(intent)
        }
    }


    override fun onRestart() {
        super.onRestart()

        val usuarioPuntos: TextView? = this.findViewById(R.id.textPuntos)
        if(puntos!="null"){
            usuarioPuntos?.text = puntos
        }

        val usuarioNivel: TextView? = this.findViewById(R.id.textNivel)

        if(puntosAcumulados!="null"){
            usuarioNivel?.text = ((Integer.parseInt(puntosAcumulados)/50)+1).toString()
        }
    }

    override fun update(o: Observable?, arg: Any?) {
        puntos= UsuarioModel.getData()!!
        puntosAcumulados= UsuarioModel.getPuntosAcumulados()!!

        val usuarioPuntos: TextView? = this.findViewById(R.id.textPuntos)
        usuarioPuntos?.text = puntos

        val usuarioNivel: TextView? = this.findViewById(R.id.textNivel)
        usuarioNivel?.text = ((Integer.parseInt(puntosAcumulados)/50)+1).toString()
    }
}

package com.reciclapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.database.*
import com.reciclapp.DataModel.AliadosModel
import com.squareup.picasso.Picasso

class AliadosActivity : AppCompatActivity() {

    lateinit var mrecylerview : RecyclerView
    lateinit var ref: DatabaseReference
    lateinit var show_progress: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_aliados)

        ref = FirebaseDatabase.getInstance().getReference().child("sponsors")
        mrecylerview = findViewById<RecyclerView>(R.id.listView)
        mrecylerview.layoutManager = LinearLayoutManager(this)

        show_progress = findViewById(R.id.progress_bar)


        firebaseData()

    }

    fun firebaseData() {


        val option = FirebaseRecyclerOptions.Builder<AliadosModel>()
            .setQuery(ref, AliadosModel::class.java)
            .setLifecycleOwner(this)
            .build()


        val firebaseRecyclerAdapter = object: FirebaseRecyclerAdapter<AliadosModel, MyViewHolder>(option) {
            
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
                val itemView = LayoutInflater.from(this@AliadosActivity).inflate(R.layout.lista_aliados,parent,false)
                return MyViewHolder(itemView)
            }

            override fun onBindViewHolder(holder: MyViewHolder, position: Int, model: AliadosModel) {
                val placeid = getRef(position).key.toString()

                ref.child(placeid).addValueEventListener(object: ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {
                        Toast.makeText(this@AliadosActivity, "Error Occurred "+ p0.toException(), Toast.LENGTH_SHORT).show()

                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        show_progress.visibility = if(itemCount == 0) View.VISIBLE else View.GONE

                        holder.txt_name.setText(model.nombre)
                        Picasso.get().load(model.logo).into(holder.img_vet)
                    }
                })
            }
        }
        mrecylerview.adapter = firebaseRecyclerAdapter
        firebaseRecyclerAdapter.startListening()
    }

    class MyViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {

        internal var txt_name: TextView = itemView!!.findViewById<TextView>(R.id.Display_title)
        internal var img_vet: ImageView = itemView!!.findViewById<ImageView>(R.id.Display_img)


    }
}


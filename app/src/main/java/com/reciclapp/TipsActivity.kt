package com.reciclapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.reciclapp.DataModel.TipModel
import java.util.*

class TipsActivity : AppCompatActivity(), Observer {

    private var mValueDataListener: ValueEventListener? = null
    lateinit var tip: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tips)

        TipModel.addObserver(this)

        tip= TipModel.getData().toString()
        Log.i("hhh", tip)

        if(tip!="null") {

            val tip: TextView? = this.findViewById(R.id.textView2)
            tip?.text = TipModel.getData().toString()
        }
    }

    override fun onRestart() {
        super.onRestart()

        tip= TipModel.getData()!!

        val tipText: TextView? = this.findViewById(R.id.textView2)
        tipText?.text = tip
    }

    override fun update(p0: Observable?, p1: Any?) {
        tip= TipModel.getData()!!

        val tipText: TextView? = this.findViewById(R.id.textView2)
        tipText?.text = tip
    }

}

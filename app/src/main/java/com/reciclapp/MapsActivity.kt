package com.reciclapp

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.Location
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.Tasks
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_maps.*
import com.google.firebase.database.DataSnapshot
import com.reciclapp.DataModel.CentroReciclaje
import com.reciclapp.DataModel.CentroReciclajeModel
import kotlinx.android.synthetic.main.activity_maps.but_inicio
import kotlinx.android.synthetic.main.activity_maps.but_puntos
import java.io.Serializable
import java.util.*
import kotlin.collections.ArrayList

class MapsActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
    Observer, Serializable {

    //fusedLocationProviderClient clase que permite acceder a la localización
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    //Propiedad que indica cual es la última localización conocida del usuario
    private lateinit var lastLocation: Location

    //Shared preferences


    companion object{

        //Representa el permiso para acceder a la localización
        private const val LOCATION_PERMISSION_REQUEST_CODE=1
    }

    //Se asigna que la clase es la que se va a encargar de lo relacionado al mapa
    override fun onMarkerClick(p0: Marker?)=false

    private lateinit var mMap: GoogleMap

    private lateinit var data: ArrayList<CentroReciclaje>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        //Se obtiene el cliente para obtener la localización
        fusedLocationClient=LocationServices.getFusedLocationProviderClient(this)

        //Instanciación del Modelo de Centros de Reciclaje
        CentroReciclajeModel.addObserver(this)
        data= ArrayList()
        data= CentroReciclajeModel.getData()!!

        but_inicio.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            // start your next activity
            startActivity(intent)
        }

        but_puntos.setOnClickListener {
            val intent = Intent(this, Activity_Puntos::class.java)
            // start your next activity
            startActivity(intent)
        }

        but_camara.setOnClickListener {
            val intent = Intent(this, Activity_Camara::class.java)
            // start your next activity
            startActivity(intent)
        }


    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.setOnMarkerClickListener(this)

        //Opciones de zoom
        mMap.uiSettings.isZoomControlsEnabled=true

        setUpMap()
    }

    private fun placeMarker(location: LatLng)
    {
        val markerOption=MarkerOptions().position(location)
        //Cambiar color marcador
        markerOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))

        mMap.addMarker(markerOption)
    }

    private fun placeMarkerCentro(location: LatLng)
    {
        val markerOption=MarkerOptions().position(location)
        //Cambiar color marcador
        markerOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))

        mMap.addMarker(markerOption)
    }

    //Callback permisos
    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            LOCATION_PERMISSION_REQUEST_CODE-> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    ubicarUsuario()
                    agregarCentros()
                    }
                else {
                    finish()
                    val snackbar = Snackbar.make(
                        root_layout,
                        "Debes dar permisos de ubicación para acceder a esta funcionalidad",
                        Snackbar.LENGTH_INDEFINITE
                    )

                    snackbar.setAction("Esconder",{
                        // Hide the snack bar
                        snackbar.dismiss()
                    })
                    // Finally, display the snack bar
                    snackbar.show()
                }
                return
            }
        }
    }

    //Mira que se tenga un permiso en especifico, en este caso access fine location
    private fun setUpMap(){
        if(ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE)
            return
        }

        ubicarUsuario()
        agregarCentros()
    }

    private fun ubicarUsuario()
    {
        //Desplegar ubicación del usuario
        mMap.isMyLocationEnabled=true

        // Cambiar tipo de mapa, Normal es el mapa regular
        mMap.mapType=GoogleMap.MAP_TYPE_NORMAL

        fusedLocationClient.lastLocation.addOnSuccessListener (this) { location->

            if(location!=null){

                lastLocation=location
                val currentLatLong=LatLng(location.latitude, location.longitude)

                //Poner marcador en ubicación actual
                placeMarker(currentLatLong)

                //Zoom, o lo más lejos, más alto más cerca
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLong, 15f))
            }
        }

    }

    private fun agregarCentros()
    {
        if(verifyAvailableNetwork(this) == false) {
            val snackbar = Snackbar.make(
                root_layout,
                "No cuentas con una conexión a internet. La información podría estar desactualizada",
                Snackbar.LENGTH_INDEFINITE
            )

            snackbar.setAction("OK",{
                // Hide the snack bar
                snackbar.dismiss()
            })

            // Finally, display the snack bar
            snackbar.show()
        }
        Log.i("Tamaño", data.size.toString())
        for (centro: CentroReciclaje in data){
            if(centro.latitud!="NA" || centro.longitud!="NA") {
                try {
                    val currentLatLong = LatLng(centro.latitud.toDouble(), centro.longitud.toDouble())
                    placeMarkerCentro(currentLatLong)
                }
                catch(e: Exception)
                {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun update(o: Observable?, arg: Any?) {
        data= CentroReciclajeModel.getData()!!
        agregarCentros()
    }

    fun verifyAvailableNetwork(activity:AppCompatActivity):Boolean{
        val connectivityManager=activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo=connectivityManager.activeNetworkInfo
        return  networkInfo!=null && networkInfo.isConnected
    }
}

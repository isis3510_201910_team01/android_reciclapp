package com.reciclapp

import android.content.Context
import android.app.Activity
import android.content.Intent
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import com.google.zxing.integration.android.IntentIntegrator
import kotlinx.android.synthetic.main.activity__camara.*

class Activity_Camara: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity__camara)

        button3.setOnClickListener {
            if(verifyAvailableNetwork(this) == false) {
                val snackbar = Snackbar.make(
                    root_layout,
                    "Parece que no tienes conexión a internet :(",
                    Snackbar.LENGTH_INDEFINITE
                )

                snackbar.setAction("Esconder",{
                    // Hide the snack bar
                    snackbar.dismiss()
                })

                // Finally, display the snack bar
                snackbar.show()
            }
            else {
                val scan = IntentIntegrator(this)

                scan.initiateScan()
            }

        }

        but_inicio.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            // start your next activity
            startActivity(intent)
        }

        but_puntos.setOnClickListener {
            val intent = Intent(this, Activity_Puntos::class.java)
            // start your next activity
            startActivity(intent)
        }

        but_centros.setOnClickListener {
            val intent = Intent(this, MapsActivity::class.java)
            // start your next activity
            startActivity(intent)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            val  result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if(result != null) {
                if(result.getContents() == null) {
                    Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(this, "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();
                }
            } else {
                super.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    fun verifyAvailableNetwork(activity:AppCompatActivity):Boolean{
        val connectivityManager=activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo=connectivityManager.activeNetworkInfo
        return  networkInfo!=null && networkInfo.isConnected
    }
}
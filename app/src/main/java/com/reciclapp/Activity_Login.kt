package com.reciclapp

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import kotlinx.android.synthetic.main.activity__login.*
import android.content.Intent
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.text.TextUtils
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import com.facebook.*
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity__login.root_layout
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_register.*
import org.json.JSONObject
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


class Activity_Login : AppCompatActivity() {

    val RC_SIGN_IN: Int = 1
    lateinit var mGoogleSignInClient: GoogleSignInClient
    lateinit var mGoogleSignInOptions: GoogleSignInOptions
    private lateinit var firebaseAuth: FirebaseAuth
    var callbackManager:CallbackManager?=null


    private lateinit var txtUser: EditText
    private lateinit var txtPassword: EditText
    private lateinit var progressBar: ProgressBar

    fun onClickFacebookButton() {

        fb_button.performClick()

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity__login)

        configureGoogleSignIn()
        setupUI()
        firebaseAuth = FirebaseAuth.getInstance()
        callbackManager = CallbackManager.Factory.create()

        fb_button.setReadPermissions("email")
        fb_button.setReadPermissions("user_birthday")
        fb_button.setReadPermissions("public_profile")
        fb_button.setOnClickListener{
            signInFB()
        }

        mi_FB_button.setOnClickListener {
            if(verifyAvailableNetwork(this) == false) {
                val snackbar = Snackbar.make(
                    root_layout,
                    "Asegúrate de tener una conexión a internet e intentalo de nuevo",
                    Snackbar.LENGTH_INDEFINITE
                )

                snackbar.setAction("OK",{
                    // Hide the snack bar
                    snackbar.dismiss()
                })

                // Finally, display the snack bar
                snackbar.show()
            }
            else {
                onClickFacebookButton()
            }
        }

        //Login normal
        txtUser=findViewById(R.id.txtUser)
        txtPassword=findViewById(R.id.txtPassword)
        progressBar= findViewById(R.id.progressBar)

    }

    //Login normal
    fun forgotPassword(view:View){
        startActivity(Intent(this, ForgotPswdActivity::class.java))
    }

    //Login normal
    fun register(view:View){
        startActivity(Intent(this, RegisterActivity::class.java))
    }

    //Login normal
    fun login(view:View){
        loginUser()
    }

    //Login normal
    private fun loginUser(){
        val user:String=txtUser.text.toString()
        val password:String=txtPassword.text.toString()

        //Se verifica que el email sea válido
        if(!EmailValidator.isEmailValid(user))
        {
            Log.d("EMAIL MALO", "Por favor ingrese un email válido");

            val snackbar = Snackbar.make(
                root_layout,
                "Por favor ingrese un email válido.",
                Snackbar.LENGTH_INDEFINITE
            )

            snackbar.setAction("OK", {
                // Hide the snack bar
                snackbar.dismiss()
            })

            // Finally, display the snack bar
            snackbar.show()
        }

        //Se verifica que no esten vacios
        if(!TextUtils.isEmpty(user) && !TextUtils.isEmpty(password))
        {
            progressBar.visibility=View.VISIBLE

            //Se inicia sesión
            firebaseAuth.signInWithEmailAndPassword(user, password).addOnCompleteListener(this){
                    task ->

                if(task.isSuccessful){
                    action()
                }else{
                    val snackbar = Snackbar.make(
                        root_layout,
                        "Error en las credenciales. Vuelve a intentarlo",
                        Snackbar.LENGTH_INDEFINITE
                    )

                    snackbar.setAction("OK",{
                        // Hide the snack bar
                        snackbar.dismiss()
                    })

                    // Finally, display the snack bar
                    snackbar.show()
                }
            }

        }
        else {
            val snackbar = Snackbar.make(
                root_layout,
                "Debes ingresar tu correo y tu contraseña",
                Snackbar.LENGTH_INDEFINITE
            )

            snackbar.setAction("OK",{
                // Hide the snack bar
                snackbar.dismiss()
            })

            // Finally, display the snack bar
            snackbar.show()
        }
    }

    //Login normal
    private fun action(){
        val actual: FirebaseUser? = firebaseAuth.currentUser

        if(actual?.isEmailVerified!!){
            startActivity(Intent(this, MainActivity::class.java))
        }else{
            val snackbar = Snackbar.make(
                root_layout,
                "Debes verificar tu email. Revisa el correo que se te envió y sigue las instrucciones. ",
                Snackbar.LENGTH_INDEFINITE
            )

            snackbar.setAction("OK",{
                // Hide the snack bar
                snackbar.dismiss()
            })

            // Finally, display the snack bar
            snackbar.show()
        }
    }

    //Validación email
    class EmailValidator {
        companion object {
            @JvmStatic
            val EMAIL_REGEX = "^[A-Za-z](.*)([@]{1})(.{1,})(\\.)(.{1,})"

            fun isEmailValid(email: String): Boolean {
                return EMAIL_REGEX.toRegex().matches(email)
            }
        }
    }

    override fun onStart() {
        super.onStart()
        val user = FirebaseAuth.getInstance().currentUser

        if(user!=null) {
            if (user?.isEmailVerified!!) {

                startActivity(MainActivity.getLaunchIntent(this))
                finish()

            }
        }

    }

    private fun configureGoogleSignIn() {
        mGoogleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, mGoogleSignInOptions)
    }

    private fun setupUI() {
        google_button.setOnClickListener {
            if(verifyAvailableNetwork(this) == false) {
                val snackbar = Snackbar.make(
                    root_layout,
                    "Asegúrate de tener una conexión a internet e intentalo de nuevo",
                    Snackbar.LENGTH_INDEFINITE
                )

                snackbar.setAction("OK",{
                    // Hide the snack bar
                    snackbar.dismiss()
                })

                // Finally, display the snack bar
                snackbar.show()
            }
            else {
                signIn()
            }
        }
    }

    private fun signIn() {
        val signInIntent: Intent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    private fun getUserProfile(currentToken: AccessToken){
        Log.i("FB", "get1")
        val request: GraphRequest= GraphRequest.newMeRequest(currentToken, GraphRequest.GraphJSONObjectCallback { jsonObject: JSONObject, response: GraphResponse ->
                try{
                    Log.i("FB", "get2")
                    val nombre=response.jsonObject.getString("first_name")
                    Log.i("FB", nombre)
                    val apellido=response.jsonObject.getString("last_name")
                    Log.i("FB", apellido)
                    val email=response.jsonObject.getString("email")
                    Log.i("FB", email)

                    //val genero=response.jsonObject.getString("gender")
                    //Log.i("FB", genero)
                    //val fecha: String=response.jsonObject.getString("birthday")

                    Log.i("FB", FirebaseAuth.getInstance().currentUser!!.uid)
                    writeNewUser(FirebaseAuth.getInstance().currentUser!!.uid, nombre + " " +apellido, email, "", "", "0", "0")
                } catch(e:Exception){
                    e.printStackTrace()
                }

        })

        val parametros: Bundle= Bundle()
        parametros.putString("fields", "first_name, last_name, email, gender, birthday")
        request.parameters=parametros
        request.executeAsync()
    }

    private fun signInFB() {
        fb_button.registerCallback(callbackManager,object:FacebookCallback<LoginResult>{
            override fun onSuccess(result: LoginResult?) {
                Log.i("FB", "pasa1")
                handleFacebookAccessToken(result!!.accessToken)
            }

            override fun onCancel() {

            }

            override fun onError(error: FacebookException?) {
                if(verifyAvailableNetwork(this@Activity_Login) == false) {
                    val snackbar = Snackbar.make(
                        root_layout,
                        "Asegúrate de tener una conexión a internet e intentalo de nuevo",
                        Snackbar.LENGTH_INDEFINITE
                    )

                    snackbar.setAction("OK",{
                        // Hide the snack bar
                        snackbar.dismiss()
                    })

                    // Finally, display the snack bar
                    snackbar.show()
                }
            }
        })

    }



    private fun handleFacebookAccessToken(accessToken: AccessToken?) {
        //Get credential
        val credential = FacebookAuthProvider.getCredential(accessToken!!.token)
        Log.i("FB", "pasa2")
        firebaseAuth!!.signInWithCredential(credential)
            .addOnFailureListener{ e->
                Toast.makeText(this,e.message,Toast.LENGTH_LONG).show()
            }
            .addOnSuccessListener { result ->
                //Get email
                Log.i("FB", "pasa3")
                val email =  result.user!!.email
                Log.i("FB", "pasa4")
                Toast.makeText(this, "You logged with email: "+email, Toast.LENGTH_LONG).show()
                Log.i("FB", "pasa5")
                getUserProfile(accessToken)
                Log.i("FB", "pasa6")
                startActivity(MainActivity.getLaunchIntent(this))
            }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //Google
        if (requestCode == RC_SIGN_IN) {
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)
                if (account != null) {
                    firebaseAuthWithGoogle(account)
                }
            } catch (e: ApiException) {
                Toast.makeText(this, "Google sign in failed:(", Toast.LENGTH_LONG).show()
            }
        }

        //FB
        callbackManager!!.onActivityResult(requestCode,resultCode,data)
    }

    data class User(
        var nombre: String? = "",
        var email: String? = "",
        var genero: String?="",
        var nacimiento: String?="",
        var puntosActuales: String?="",
        var puntosAcumulados: String?=""

    )

    //Funcion para registrar el usuario
    private fun writeNewUser(userId: String, nombre: String, email: String?, genero: String?, nacimiento: String?, puntosActuales: String?, puntosAcumulados: String? ) {
        val user = User(nombre, email, genero, nacimiento, puntosActuales, puntosAcumulados)
        FirebaseDatabase.getInstance().reference.child("usuarios").child(userId).setValue(user)
    }

    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {
        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener {
            if (it.isSuccessful) {

                val acct = GoogleSignIn.getLastSignedInAccount(this)

                if (acct != null) {
                    Log.i("PASA", "4")
                    val personName = acct.displayName
                    val personGivenName = acct.givenName
                    val personFamilyName = acct.familyName
                    val personEmail = acct.email
                    val personId = acct.id
                    val personPhoto = acct.photoUrl
                    Log.i("PASA", "5")
                    Log.i("PASA", FirebaseAuth.getInstance().currentUser!!.uid)
                    writeNewUser(FirebaseAuth.getInstance().currentUser!!.uid, personGivenName+ " "+ personFamilyName, personEmail, " ", " ", "0", "0")
                    Log.i("PASA", "6")
                }

                startActivity(MainActivity.getLaunchIntent(this))
            } else {
                Toast.makeText(this, "Google sign in failed:(", Toast.LENGTH_LONG).show()
            }
        }
    }

    fun verifyAvailableNetwork(activity:AppCompatActivity):Boolean{
        val connectivityManager=activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo=connectivityManager.activeNetworkInfo
        return  networkInfo!=null && networkInfo.isConnected
    }

    companion object {
        fun getLaunchIntent(from: Context) = Intent(from, Activity_Login::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        }
    }
}

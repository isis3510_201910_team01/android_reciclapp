package com.reciclapp

import android.app.DatePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.*
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.google.firebase.auth.FirebaseAuthWeakPasswordException
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_register.*
import java.util.*

class RegisterActivity : AppCompatActivity() {

    private lateinit var txtName: EditText
    private lateinit var txtLastName: EditText
    private lateinit var txtEmail: EditText
    private lateinit var txtPassword: EditText
    private lateinit var txtPassword2: EditText
    private lateinit var dbReference: DatabaseReference
    private lateinit var database: FirebaseDatabase
    private lateinit var auth: FirebaseAuth

    //Fecha
    var c= Calendar.getInstance()
    var year=c.get(Calendar.YEAR)
    var month=c.get(Calendar.MONTH)
    var day=c.get(Calendar.DAY_OF_MONTH)

    //Género
    lateinit var radioGroup:RadioGroup

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        txtName=findViewById(R.id.txtName)
        txtLastName=findViewById(R.id.txtLastName)
        txtEmail=findViewById(R.id.txtEmail)
        txtPassword=findViewById(R.id.txtPassword)
        txtPassword2=findViewById(R.id.txtPassword2)

        database= FirebaseDatabase.getInstance()
        auth=FirebaseAuth.getInstance()

        dbReference=database.reference.child("usuarios")


        //fecha2
        txtBirthDate2.setOnClickListener{
            val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener{ view: DatePicker, mYear:Int, mMonth:Int, mDay:Int ->
                val mMes = mMonth+1
                txtBirthDate2.setText(""+mDay+"/"+mMes+"/"+mYear)
                year=mYear
                month=mMes
                day=mDay
            }, year, month-1, day)

            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)

            c.set(Calendar.YEAR, year - 15)
            dpd.datePicker.maxDate = c.timeInMillis

            dpd.show()
        }



        //radioGroup
        radioGroup=findViewById(R.id.radioGroup)
    }

    fun register(view: View){
        createNewAccount()
    }

    private fun createNewAccount() {
        //Se obtienen los valores de los campos
        val name: String = txtName.text.toString()
        val lastName: String = txtLastName.text.toString()
        val email: String = txtEmail.text.toString()
        val password: String = txtPassword.text.toString()
        val password2: String = txtPassword2.text.toString()
        var date=""

        if(day!==null && month!==null && year!=null) {
            date = "" + day + "/" + month + "/" + year
        }

        var id =radioGroup.checkedRadioButtonId
        var valGen=findViewById<RadioButton>(id).text.toString()

        //Se verifica email
        if(!EmailValidator.isEmailValid(email))
        {
            Log.d("EMAIL MALO", "Por favor ingrese un email válido");

            val snackbar = Snackbar.make(
                root_layout,
                "Por favor ingrese un email válido.",
                Snackbar.LENGTH_INDEFINITE
            )

            snackbar.setAction("OK", {
                // Hide the snack bar
                snackbar.dismiss()
            })

            // Finally, display the snack bar
            snackbar.show()
        }

        //Se verifica las contraseñas
        if(password!=password2)
        {
            Log.d("NO COINCIDE CONTRASENIA", "Las contraseñas no coinciden.");

            val snackbar = Snackbar.make(
                root_layout,
                "Las contraseñas no coinciden.",
                Snackbar.LENGTH_INDEFINITE
            )

            snackbar.setAction("OK", {
                // Hide the snack bar
                snackbar.dismiss()
            })

            // Finally, display the snack bar
            snackbar.show()
        }

        //Se verifican que no esten vacios
        if (!TextUtils.isEmpty(name) && !TextUtils.isEmpty(lastName) && !TextUtils.isEmpty(email) && !TextUtils.isEmpty(
                password)&& date!=="" && !TextUtils.isEmpty(valGen) && EmailValidator.isEmailValid(email) && password==password2) {

            auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this) { task ->

                    if(!task.isSuccessful)
                    {
                        try{
                            throw task.getException()!!
                        }
                        catch (e: FirebaseAuthUserCollisionException)
                        {
                            Log.d("COLISION", "El email ya existe");

                            val snackbar = Snackbar.make(
                                root_layout,
                                "Ya hay un cuenta con el email ingresado. ",
                                Snackbar.LENGTH_INDEFINITE
                            )

                            snackbar.setAction("OK", {
                                // Hide the snack bar
                                snackbar.dismiss()
                            })

                            // Finally, display the snack bar
                            snackbar.show()
                        }
                        catch (e: FirebaseAuthWeakPasswordException)
                        {
                            Log.d("CLAVE DEBIL", "La clave debe tener por lo menos 6 caracteres.");

                            val snackbar = Snackbar.make(
                                root_layout,
                                "La clave debe tener al menos 6 caracteres.",
                                Snackbar.LENGTH_INDEFINITE
                            )

                            snackbar.setAction("OK", {
                                // Hide the snack bar
                                snackbar.dismiss()
                            })

                            // Finally, display the snack bar
                            snackbar.show()
                        }

                    }else if(task.isComplete) {
                        val user: FirebaseUser? = auth.currentUser
                        verifyEmail(user)

                        val userBD=dbReference.child(user?.uid!!)

                        userBD.child("nombre").setValue(name+" "+lastName)
                        userBD.child("email").setValue(email)
                        userBD.child("nacimiento").setValue(date)
                        userBD.child("genero").setValue(valGen)
                        userBD.child("puntosActuales").setValue(0)
                        userBD.child("puntosAcumulados").setValue(0)
                        action()

                    }
                }
        }
        else{
            val snackbar = Snackbar.make(
                root_layout,
                "Debe completar todos los campos",
                Snackbar.LENGTH_INDEFINITE
            )

            snackbar.setAction("OK", {
                // Hide the snack bar
                snackbar.dismiss()
            })

            // Finally, display the snack bar
            snackbar.show()
        }
    }

    private fun action(){
        val snackbar = Snackbar.make(
            root_layout,
            "Por favor verificar su cuenta de email a través del correo enviado",
            Snackbar.LENGTH_INDEFINITE
        )

        snackbar.setAction("OK", {
            // Hide the snack bar
            snackbar.dismiss()
            startActivity(Intent(this, Activity_Login::class.java))
        })

        // Finally, display the snack bar
        snackbar.show()

    }

    private fun verifyEmail(user:FirebaseUser?){

        user?.sendEmailVerification()?.addOnCompleteListener(this){
                task ->

            if(task.isComplete) {
                //No se
            }else{
                val snackbar = Snackbar.make(
                    root_layout,
                    "Error al enviar el email de verificación. Vuelva a intentarlo",
                    Snackbar.LENGTH_INDEFINITE
                )

                snackbar.setAction("OK", {
                    // Hide the snack bar
                    snackbar.dismiss()
                })

                // Finally, display the snack bar
                snackbar.show()
            }
        }

    }

    //Validación email
    class EmailValidator {
        companion object {
            @JvmStatic
            val EMAIL_REGEX = "^[A-Za-z](.*)([@]{1})(.{1,})(\\.)(.{1,})"

            fun isEmailValid(email: String): Boolean {
                return EMAIL_REGEX.toRegex().matches(email)
            }
        }
    }
}

package com.reciclapp

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import android.content.Intent
import android.util.Log
import android.widget.ListView
import android.widget.TextView
import com.reciclapp.Adapters.UsuarioAdapter
import com.reciclapp.DataModel.Usuario
import com.reciclapp.DataModel.UsuarioModel
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity(), Observer {

    lateinit var puntos: String
    lateinit var puntosAcumulados: String

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        UsuarioModel.addObserver(this)

        puntos= UsuarioModel.getData().toString()
        Log.i("hhh", puntos)

        puntosAcumulados=UsuarioModel.getPuntosAcumulados().toString()

        if(puntos!="null")
        {
            val usuarioPuntos: TextView? = this.findViewById(R.id.textPuntos)
            usuarioPuntos?.text = puntos

            val usuarioNivel: TextView? = this.findViewById(R.id.textNivel)
            usuarioNivel?.text = ((Integer.parseInt(puntosAcumulados)/50)+1).toString()
        }

        but_nivel.setOnClickListener {
            val intent = Intent(this, Activity_Puntos::class.java)
            // start your next activity
            startActivity(intent)
        }


        but_puntos.setOnClickListener {
            val intent = Intent(this, Activity_Puntos::class.java)
            // start your next activity
            startActivity(intent)
        }

        but_centros.setOnClickListener {
            val intent = Intent(this, MapsActivity::class.java)
            // start your next activity
            startActivity(intent)
        }

        but_camara.setOnClickListener {
            val intent = Intent(this, Activity_Camara::class.java)
            // start your next activity
            startActivity(intent)
        }

        buttonTips.setOnClickListener {
            val intent = Intent(this, TipsActivity::class.java)
            // start your next activity
            startActivity(intent)
        }
    }

    override fun onRestart() {
        super.onRestart()

        val usuarioPuntos: TextView? = this.findViewById(R.id.textPuntos)
        if(puntos!="null"){
            usuarioPuntos?.text = puntos
        }

        val usuarioNivel: TextView? = this.findViewById(R.id.textNivel)

        if(puntosAcumulados!="null"){
            usuarioNivel?.text = ((Integer.parseInt(puntosAcumulados)/50)+1).toString()
        }

    }

    override fun update(p0: Observable?, p1: Any?) {
        puntos= UsuarioModel.getData()!!
        puntosAcumulados= UsuarioModel.getPuntosAcumulados()!!

        val usuarioPuntos: TextView? = this.findViewById(R.id.textPuntos)
        usuarioPuntos?.text = puntos

        val usuarioNivel: TextView? = this.findViewById(R.id.textNivel)
        usuarioNivel?.text = ((Integer.parseInt(puntosAcumulados)/50)+1).toString()
    }

    companion object {
        fun getLaunchIntent(from: Context) = Intent(from, MainActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        }
    }
}
